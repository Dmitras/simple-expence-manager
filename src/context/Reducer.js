import {SAVE_TRANSACTION, DELETE_TRANSACTION } from "./Actions";


export default (state, action) => {
	const {payload, type} = action;

	switch (type) {
		case SAVE_TRANSACTION:
			return {
				...state,
				transactions: state.transactions.concat(payload)
			}
		case DELETE_TRANSACTION:
			return {
				...state,
				transactions: state.transactions.filter(transaction => transaction.id !== action.payload)
			}
		default:
			return state;
	}
}

