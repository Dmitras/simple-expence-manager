import React, { createContext, useReducer } from 'react';
import Reducer from './Reducer';
import { SAVE_TRANSACTION, DELETE_TRANSACTION} from './ActionTypes';
// import {saveTransaction, deleteTransaction, SAVE_TRANSACTION, DELETE_TRANSACTION} from './Actions';


const initialState = {
	transactions: [
		{id:1, text: 'Salary', amount: 2000 },
		{id:2, text: 'Fuel', amount: -200 },
		{id:3, text: 'Food', amount: -400 },
		{id:4, text: 'Headphones', amount: -250 },
		{id:5, text: 'Boots', amount: -200 },
	]
}

export const GlobalContext = createContext(initialState);

export const GlobalProvider = ({children}) => {
	const [state, dispatch] = useReducer(Reducer, initialState)

	const addTransaction = transaction => dispatch({
		type: SAVE_TRANSACTION,
		payload: transaction
	})

	const deleteTransaction = id => dispatch({
		type: DELETE_TRANSACTION,
		payload: id
	})


	return (
		<GlobalContext.Provider value={{
			transactions: state.transactions,
			deleteTransaction,
			addTransaction
		}}>
			{children}
		</GlobalContext.Provider>
	)
}