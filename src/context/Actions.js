import {GlobalProvider} from './State'

export const SAVE_TRANSACTION = "SAVE_TRANSACTION";
export const DELETE_TRANSACTION = "DELETE_TRANSACTION";

export const saveTransaction = transaction => dispatch => ({
	type: SAVE_TRANSACTION,
	payload: transaction
})

export const deleteTransaction = id => dispatch => ({
	type: DELETE_TRANSACTION,
	payload: id
})