import React, {Component} from 'react';
import Header from '/src/components/Header';
import Balance from '/src/components/Balance';
import IncomeExpenses from '/src/components/IncomeExpenses';
import './styles/App.css';
import TransactionList from "./components/TransactionList";
import AddTransaction from "./components/AddTransaction";
import { GlobalProvider } from "./context/State";

class App extends Component {
	render() {
		return (
			<GlobalProvider>
				<Header />
				<div className="container">
					<Balance />
					<IncomeExpenses />
					<TransactionList />
					<AddTransaction />
				</div>
			</GlobalProvider>
		)
	}
}

export default App;