import React, {useContext} from 'react';
import { GlobalContext } from "../context/State";

export const Transaction = ({transaction}) => {
	const { deleteTransaction } = useContext(GlobalContext);

	return (
		<li key={transaction.id} className={transaction.amount >= 0 ? 'plus' : 'minus'}>
			{transaction.text}
			<span>{transaction.amount}</span>
			<button className='delete-btn'
							onClick={() => deleteTransaction(transaction.id)}
			>X</button>
		</li>
	)
}

