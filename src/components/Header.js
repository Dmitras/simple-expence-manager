import React from 'react';

const Header = () => {
	return (
		<h2>
			Expense manager
		</h2>
	);
};

export default Header;