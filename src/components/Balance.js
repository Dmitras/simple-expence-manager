import React, { useContext } from 'react';
import {GlobalContext} from "../context/State";

const Balance = () => {
	const { transactions } = useContext(GlobalContext)
	const amounts = transactions.map(transaction => transaction.amount)
	const total = amounts.reduce((acc, curr) => acc + curr, 0)
	const normalizedTotal = Math.sqrt(total ** 2) /** or Math.abs **/
	const sign = total > 0 ? '' : '-'

	return (
		<>
			<h4>Your Balance</h4>
			<h1>{sign}${normalizedTotal}</h1>
		</>
	);
};

export default Balance;