const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin');

const isDevelopment = process.env.NODE_ENV !== 'production';

module.exports = {
	entry: "./src/index.js",
	output: {
		path: path.join(__dirname, "/dist"),
		filename: "bundle.js"
	},
	devtool: isDevelopment && "source-map",
	devServer: {
		port: 3001,
		open: true,
		contentBase: path.join(__dirname, "../src"),
		historyApiFallback: true,
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader"
				},
			},
			{
				test: /\.css$/,
				use: ["style-loader", "css-loader"]
			}
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: "./public/index.html"
		}),
	]
}